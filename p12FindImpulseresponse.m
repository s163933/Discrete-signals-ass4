close all

if((exist("Toolbox", 'dir')) == 7)
    addpath("Toolbox")
elseif (exist("Discrete_Signals_Ass2/Toolbox",'dir') == 7)
    addpath("Discrete_Signals_Ass2/Toolbox");
end

%% Load audio
% Don't do more than 18 files
lengths = [100, 1000, 10000, 100000, 500000];
impulses = zeros(max(lengths),length(lengths));
for i=1:length(lengths)
    INFILE = sprintf('in_length_%d.wav', lengths(i));
    OUTFILE = sprintf('out_length_%d.wav', lengths(i));
    disp(INFILE);
    % Load audio
    [in_sig, fs] = audioread(INFILE);
    [out_sig, ~] = audioread(OUTFILE);

    % Do things
    fftIn = fft(in_sig);
    fftOut = fft(out_sig);

    math = fftOut./fftIn;

    figure(i)
    subplot(2,1,1);
    plot(mag2db(abs(in_sig)));
    hold on
    plot(mag2db(abs(out_sig)));
    plot(mag2db(abs(math)));
    legend("Input signal", "Output signal", "Output over input signal", 'location', 'northwest');
    ylim([-20 30]);
    hold off

    subplot(2,1,2);
    impulse = ifft(math);
    impulses(1:length(impulse),i) = impulse;
    plot(impulse);
    xlim([0 300])
end

figure(20)
ticks = 0:2/length(math):2-1/length(math);
plot(ticks,mag2db(abs(math)));
xlim([0 1])
ylim([-20 30])
xlabel("Normalised frequency (\pi rad/sample)");
ylabel("Magnitude (dB)");
title("Magnitude plot of impulse response, n=500000");
set(gca, 'FontSize', 15);
saveas(gca, 'bodeMag.eps', 'epsc');

%% Compare impulse responses

figure(19) % Why 18 files is the limit
for i=1:min(size(impulses))
    TEMPNAME = sprintf('Length %d', lengths(i));
    plot(impulses(:,i),'DisplayName',TEMPNAME);
    hold on
end
hold off
xlim([0 180]);
xlabel("Samples");
legend
title("Comparison of impulse responses from different signal lengths");
set(gca, 'FontSize', 15);
saveas(gca, 'impulseResponses.eps', 'epsc');


