close all

if((exist("Toolbox", 'dir')) == 7)
    addpath("Toolbox")
elseif (exist("Discrete_Signals_Ass2/Toolbox",'dir') == 7)
    addpath("Discrete_Signals_Ass2/Toolbox");
end

% Load file
[call, fs] = audioread('animal_call.wav');

% Hear it
sound(call, fs);

% Check the spectro domain
[spectrum, handle] = hmspectogram(call,@hann, 300, 90,fs);

% That's a zebra
title("Spectogram of animal call using a Hann window, size 300, 90% overlap")
saveas(handle, 'zerbra.eps', 'epsc');
