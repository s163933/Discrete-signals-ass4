function [h,a] = bassandtreb(Wc,G,n)
%BASSANDTREB Summary of this function goes here
%   Detailed explanation goes here
% Wc - Cutoff array (normalized) [Wc_low Wc_high]
% G - Gain in dB
% n - Order of the filter 
a = 1;

l = n;

ulige = mod(l,2);

H = ones(1,l);


if ulige == 1
    i_wc = ceil(Wc*l/2);
    Wlow1 = i_wc(1)+1;
    Whigh1 = i_wc(2);
    Wlow2 = l-i_wc(1)+1;
    Whigh2 = l-i_wc(2)+2;
else % Hvis lige / even numbered l
    i_wc = ceil(Wc*l/2);
    Wlow1 = i_wc(1)+1;
    Whigh1 = i_wc(2);
    Wlow2 = l-i_wc(1)+1;
    Whigh2 = l-i_wc(2)+2;
end
H(Wlow1:Whigh1) = db2mag(G);
H(Wlow2:Whigh2) = db2mag(G);

h = ifft(H, 'symmetric');
% h = ifft(H);
if ulige == 1
    h = circshift(h, (n-1)/2);
else
    h = circshift(h, (n)/2);
end
% Should maybe circ-shift h with l/2 * n
% see results in handson8
% Hanning for niceness
curve = zeros(1,l);
w_L = l;
curve(1:w_L) = hann(w_L);
h = h .* curve;

end

