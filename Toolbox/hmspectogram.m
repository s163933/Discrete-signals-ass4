function [spectrum, figurehandle] = hmspectogram(signal,windowType,windowLength,overlap, fs, fftPoints)
%HMSPECTOGtypeRAM Summary of this function goes here
%   windowType, do ex. @hann. overlap is in %, eg 40 = 40% = 0.4

if ~exist('fftPoints','var')
     % third parameter does not exist, so default it to something
      fftPoints = windowLength;
end
fsthere = true;
if ~exist('fs','var')
     % third parameter does not exist, so default it to something
      fsthere = false;
end

shift_i = round((1-overlap*0.01)*windowLength);
overlap_i = round(windowLength*0.01*overlap);

% disp(overlap_i);
sig = signal;

% disp(windowLength);
% disp(windowType);
window = windowType(windowLength);

% shifties = floor(length(sig)/shift_i);
% Currently i guess we trunctate the last shift if it's not a full shift.

spectrum = [];

% Debug
% disp(shift_i);
% disp(shifties);
% disp(length(sig));

for k = 1:shift_i:length(sig)-windowLength
    % Debug
%     disp(k)
%     disp(k+windowLength)
    frame_start = 1;
    frame_stop = windowLength;
    frame = sig(frame_start:frame_stop);
    frame = frame .* window;
    abs_frame = abs(fft(frame,fftPoints));
    abs_frame = abs_frame*2;
    new_spec = abs_frame(round(length(abs_frame)/2):end);
    spectrum = [spectrum mag2db(new_spec)];
    sig = circshift(sig, shift_i);
end

figurehandle = imagesc(spectrum);
xlabel("Samples");
ylabel("Frequency");
set(gca, 'FontSize', 15);

if fsthere
    frequencies = fs*(0:windowLength/2)/windowLength;
    % f = Fs*(0:(L/2))/L;
    % Probably wrong :(
    tickkies = frequencies(round(length(frequencies)/10):round(length(frequencies)/10):end);
    yticklabels(tickkies);
else
    frequencies = (0:windowLength/2)/windowLength;
end
% Problably right :D
colorbar;
c = colorbar;
c.Label.String = "Magnitude (dB)";

% Good job me
spectrum = frequencies;
end

